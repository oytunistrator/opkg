package olpfile

import (
	"encoding/json"
	"os"
	"fmt"
	"io/ioutil"
	"github.com/olproject/olang/color"
	"gopkg.in/src-d/go-git.v4"
	"bufio"
	"log"
	"bytes"
	"regexp"
	"strings"
	"path/filepath"
	"os/exec"
	"github.com/olproject/olang/lexer"
	"github.com/olproject/olang/parser"
	"github.com/fsnotify/fsnotify"
)

type OlpParsed struct{
	Name    	string			`json:"package"`
	Publisher	Publisher		`json:"publisher"`
	Sources		string			`json:"sources"`
	Main		string			`json:"main"`
	Requires	[]Require		`json:"requires"`
	Compiled	string			`json:"compiled"`
	Install		[]InstallTask	`json:"install"`
	WatchList		FileWatcher	`json:"watch"`
	Jobs		[]Job	`json:"jobs"`
	Type		string			`json:"type"`
}

type Require struct{
	Name	string	`json:"name"`
	Src 	string	`json:"src"`
}

type Job struct{
	Name	string	`json:"name"`
	Command 	string	`json:"command"`
}
type InstallTask map[string]string

type Publisher struct{
	Name    	string	`json:"name"`
	Email		string	`json:"email"`
	Github		string	`json:"github"`
}


/* Add File Watcher */
type FileWatcher struct{
	Folder	string	`json:"folder"`
	Output 	string	`json:"output"`
}

type Olpfile struct{
	filename string
	path string
}

type CompiledFile struct{
	FileName string
}

var watcher *fsnotify.Watcher
var watchedFiles []string

func New(filename string, path string) *Olpfile{
	if Exists(filename) {
		return &Olpfile{ filename: filename, path: path }
	}
	return &Olpfile{ filename: "", path: "" }
}

func Exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func (olp *Olpfile) Parse() OlpParsed{
	raw, err := ioutil.ReadFile(olp.filename)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	var parsed OlpParsed

	json.Unmarshal(raw, &parsed)

	return parsed
}

func (olp *Olpfile) Install() bool{
	if !Exists(olp.filename) {
		return false
	}

	autoload := ""

	if !Exists(olp.path){
		color.Print(color.Blue("[!] INFO: \""+olp.path+"\" path not found. Path creating..."))
		os.Mkdir(olp.path, 0777)
		color.Print(color.Grey("[ready]\n"))
	}
	parseOlp := olp.Parse()
	for _, r := range parseOlp.Requires {
		olp.Clone(r.Name, r.Src)

		if Exists(olp.path+"/"+r.Name+"/"+olp.filename) {
			subOlp := New(olp.path+"/"+r.Name+"/"+olp.filename, olp.path+"/"+r.Name)
			parseOlp := subOlp.Parse()

			res := subOlp.CompileVendors(subOlp.path+"/"+parseOlp.Main, subOlp.path+"/"+parseOlp.Compiled, parseOlp.Sources, parseOlp.Name)

			if res{
				color.Print(color.Grey("[done]\n"))
				file, err := os.Open(subOlp.path+"/"+parseOlp.Compiled+"/"+parseOlp.Name+".ola")
				if err != nil {
					log.Fatal(err)
				}
				scanner := bufio.NewScanner(file)
				//env := object.NewEnvironment()

				for scanner.Scan() {
					line := scanner.Text()
					autoload += line+"\n"
				}
			}else{
				color.Print(color.Grey("[fail]"))
			}
		}
	}
	color.Printf("[>] Writing Autoload... [vendor/olp.ola]")
	olp.WriteAutoLoad(autoload, "vendor/olp.ola")
	color.Print(color.Grey("[done]\n"))
	color.Print(color.Green("[X] Fetch Completed."))
	fmt.Printf("\n")
	return true
}

func (olp *Olpfile) Clone(name string, url string){
	color.Printf("[>] Fetching\t["+color.Blue("%s")+"]", name)
	if Exists(olp.path+"/"+name){
		r, _ := git.PlainOpen(olp.path+"/"+name)
		w, _ := r.Worktree()
		w.Pull(&git.PullOptions{RemoteName: "origin"})
		color.Print(color.Green("[updated]"))

	}else{
		_, err := git.PlainClone(olp.path+"/"+name, false, &git.CloneOptions{
			URL:               url,
			RecurseSubmodules: git.DefaultSubmoduleRecursionDepth,
		})


		if err == nil{
			color.Print(color.Grey("[done]"))
		}else{
			color.Print(color.Red("[fail]"))
		}
	}

	fmt.Printf("\n")
}

func (olp *Olpfile) WriteAutoLoad(content string, autoload string){
	f, _ := os.Create(autoload)
	defer f.Close()
	f.WriteString(content)
	f.Sync()
}

func (olp *Olpfile) CompileVendors(filename string, out string, src string, packt string) bool{
	color.Printf("[>] Compiling\t["+color.Blue("%s")+"]", filename)
	result := FileParse2(filename)
	os.Mkdir(out, 0777)

	f, _ := os.Create(out+"/"+packt+".ola")
	f.WriteString(result+"\n")
	f.Sync()

	return true
}

func (olp *Olpfile) CompileProject(filename string, out string, packt string) bool{
	color.Printf("[>] Compiling\t["+color.Blue("%s")+"]", filename)
	
	
	result := FileParse2(filename)


	f, _ := os.Create(out+"/"+packt+".ola")
	f.WriteString(result+"\n")
	f.Sync()
	os.Chmod(out+"/"+packt+".ola", 0777)

	return true
}

func (olp *Olpfile) WriteOlpFile(parser OlpParsed) bool{
	f, _ := os.Create(olp.filename)

	marshallJson, _ := json.Marshal(parser)

	f.WriteString(jsonPrettyPrint(string(marshallJson)))

	f.Sync()

	return true
}

func FileParse2(filename string) string{
	content, _ := ioutil.ReadFile(filename)
	
	contstr := string(content)
	
	if len(contstr) > 0 {
		var re = regexp.MustCompile(`(?m)\#[\s\S]*?.*|\#\![\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$`)
		// \#[\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$
		
		var rel = regexp.MustCompile(`(?mi)(?:load)+\s\S+`)
		var loadRemove = regexp.MustCompile(`(?mi)(?:load)\W`)
		
		loads := rel.FindAllString(contstr, -2)
		
		loaded := ""
		for _,load := range loads{
			load = loadRemove.ReplaceAllString(load, "$1W")
			load = strings.Replace(load, "\"", "", -2)
			curDir, _ := os.Getwd()
			loadedContent, _ := ioutil.ReadFile(curDir+"/"+load)
			loaded += string(loadedContent)
		}
		
		contstr = rel.ReplaceAllString(contstr, loaded)
		contstr = re.ReplaceAllString(contstr, "$1W")
		contstr = strings.Replace(contstr, "\n", "", -2)
		contstr = strings.Replace(contstr, "\t", "", -2)
		contstr = strings.Replace(contstr, "  ", " ", -2)
		
	}	
	return contstr;
}

func (olp *Olpfile) JobRun (jobname string) {
	parseOlp := olp.Parse()
	for _, j := range parseOlp.Jobs {
		if j.Name == jobname{
			split := strings.Split(j.Command, " ")
			first := split[0]
			_, split = split[0], split[1:]
			args := strings.Join(split, " ")
			log.Println("Opkg Running... ["+j.Name+"]["+j.Command+"]")
			cmd := exec.Command(first, args)
			cmd.Run()
		}
	}
}

func ParseFile(filename string) string{
	content, _ := ioutil.ReadFile(filename)	
	contstr := string(content)
	jsResult := ""	
	
	if len(contstr) > 0 {	
		var re = regexp.MustCompile(`(?m)\#[\s\S]*?.*|\#\![\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$`)
		// \#[\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$
		
		var rel = regexp.MustCompile(`(?mi)(?:load)+\s\S+`)
		var loadRemove = regexp.MustCompile(`(?mi)(?:load)\W`)
		
		loads := rel.FindAllString(contstr, -2)
		
		loaded := ""
		for _,load := range loads{
			load = loadRemove.ReplaceAllString(load, "$1W")
			load = strings.Replace(load, "\"", "", -2)
			curDir, _ := os.Getwd()
			loadedContent, _ := ioutil.ReadFile(curDir+"/"+load)
			loaded += string(loadedContent)
		}
		
		contstr = rel.ReplaceAllString(contstr, loaded)
		contstr = re.ReplaceAllString(contstr, "\n")
		contstr = strings.Replace(contstr, "\n", "", -2)
		contstr = strings.Replace(contstr, "\t", "", -2)
		contstr = strings.Replace(contstr, "  ", " ", -2)

		l := lexer.New(contstr)
		p := parser.New(l)
		
		program := p.ParseProgram()
		
		
		jsResult = "(function(window,document){'use strict';"+program.Javascript()+"}(window,document));"
	}
	return jsResult
}


func (olp *Olpfile) Watch(folder string, output string){
	log.Println("generating:", output)
	MakeBundle(folder, output)
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
			log.Fatal(err)
	}
	defer watcher.Close()
	
	done := make(chan bool)
	go func() {
			for {
					select {
					case event := <-watcher.Events:
							log.Println("event:", event)
							if event.Op&fsnotify.Write == fsnotify.Write {
									log.Println("modified file:", event.Name)
									MakeBundle(folder, output)
									log.Println("regenerating:", output)
							}
					case err := <-watcher.Errors:
							log.Println("error:", err)
					}
			}
	}()



	err = watcher.Add(folder)
	if err != nil {
			log.Fatal(err)
	}
	<-done
}

func MakeBundle(folder string, output string){
	err := filepath.Walk(folder, func(path string, info os.FileInfo, err error) error {
			watchedFiles = append(watchedFiles, path)
			return nil
	})
	if err != nil {
			panic(err)
	}
	jsonResult := ""
	for _, file := range watchedFiles {
		jsonResult = ParseFile(file)
		data := []byte(jsonResult)
		os.Remove(output)
		f, _ := os.Create(output)
		f.Write(data)
	}
}


func jsonPrettyPrint(in string) string {
	var out bytes.Buffer
	err := json.Indent(&out, []byte(in), "", "\t")
	if err != nil {
		return in
	}
	return out.String()
}